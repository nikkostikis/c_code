//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <string.h>

void last_char(char* data);

//replace with main
int exer_13(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {
    
    
    char data1[150];
    char data2[150];
    
    printf("Give me a string...\n");
    scanf("%s", data1);
    strcpy(data2, data1); //copy the initial input to check if it works
    
    printf("You gave me...\n%s\n", data2);
    last_char(data1);
    printf("I've turned it into...\n%s\n", data1);
    
    return 0;
}
void last_char(char* data) {
    int count = 0;
    int i, rear, end;
    //first count the letters
    for (i = 0; data[i] != '\0'; i++) {
        count++;
    }
    //initialize two counters, one for the rear and one for the end of the string
    rear = 0;
    end = count - 1;
    //increment only the rear index until it reaches the end
    while (rear < end) {
        data[rear] = data[end];
        rear++;
    }
}
