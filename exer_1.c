//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

//declare a function prototype
void changeDiag(int pArray[10][10]);

//replace with main
int exer_1(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {
    
    int A[10][10];
    //int B[10];
    int num;
    int i, j;
    
    //read a 2D int array 10x10
    for (i = 0; i < 10; i++) {
        for (j = 0; j < 10; j++) {
            printf("[%d][%d]: ", i, j);
            scanf("%d", &num);
            A[i][j] = num;
        }
    }

    
    
    //call the function
    changeDiag(A);
    
    //print the array
    for (i = 0; i < 10; i++) {
        for (j = 0; j < 10; j++) {
            printf("%d ", A[i][j]);
        }
        printf("\n");
    }
    
    
    return 0;
}

//the function
void changeDiag(int pArray[10][10]){

    //diagonal elements have equal indices
    for (int i = 0; i < 10; i++)
        //put a random number in diagonal elements
        pArray[i][i] = rand();
}



