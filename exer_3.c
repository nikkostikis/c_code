//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <string.h>

int instCount(char s1[], char s2[]);

//replace with main
int exer_3(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {
    
    char ss1[150];
    char ss2[150];
    
    
    //read the two strings
    printf("string 1...\n");
    scanf("%s", ss1);
    
    printf("string 2...\n");
    scanf("%s", ss2);
    
    int count = instCount(ss1, ss2);
    
    printf("%d\n", count);
    
    return 0;
}

int instCount(char s1[], char s2[]) {
    int count=0;

    
    //loop looking for the s2 in s1

    for (int i = 0; i < strlen(s1); i++) {
        int equal = 1; //flag type variable
        for(int j = 0; j < strlen(s2); j++) {
            //if it enters this condition it's not a match

            if(s1[i+j] != s2[j]) {
                equal = 0;
                break;
            }
        }
        count += equal;
    }
    
    return count;
}

