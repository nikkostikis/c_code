//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int oddOrNot(int k);


//replace with main
int exer_24(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {
    
    int res, n;
    char *lbl = malloc(20 * sizeof(char));
    //read the 2 int values
    printf("Give me an integer...\n");
    scanf("%d", &n);
    
    
    res = oddOrNot(n); //call the function
    
    //use ternary operator (_?_:_) to define a label (lbl) as "even" or "odd"
    lbl = (res == 1)? "even": "odd";
    printf("\nResult is: %s\n", lbl);
    
    return 0;
}

int oddOrNot(int k) {
    if (k == 0) return 1;
    else return (oddOrNot(abs(k)-1) == 1? 0: 1);
}