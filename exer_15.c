//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <string.h>

void deleteSpace(char* data);

//replace with main
int exer_15(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {
    
    
    char data1[150];
    char data2[150];
    
    printf("Give me a string with spaces...\n");
    fgets(data1, sizeof(data1), stdin);
    strcpy(data2, data1); //copy the initial input to check if it works
    
    printf("You gave me...\n%s\n", data2);
    deleteSpace(data1);
    printf("I've turned it into...\n%s\n", data1);
    
    return 0;
}
void deleteSpace(char* data) {
    int i;
    int pos = 0;
    char old[150];
    
    //copy the string
    strcpy(old, data);

    for (i = 0; old[i] != '\0'; i++) {
        //copy the letter only if it's not a space
        if (old[i] != ' ') {
            data[pos] = old[i];
            pos++;
        }
    }
    data[pos] = '\0'; //manually close the string
}
