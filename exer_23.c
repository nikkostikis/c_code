//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int sum(int a[], int pos);


//replace with main
int exer_23(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {

    int res, i;
    int array[10];
    int size = 10;
    
    //read the 10 int values
    printf("Give me 10 integers...\n");
    for (i = 0; i < size ; i++) {
        scanf("%d", &array[i]);
    }
    
    //print the 10 int values
    printf("You gave me...\n");
    for (i = 0; i < size ; i++) {
        printf("%d\n", array[i]);
    }
    
    res = sum(array, size-1); //call the function
    
    printf("\nThe sum of the array is: %d\n", res);
    
    return 0;
}

int sum(int a[], int pos) {
    //base case all the elements have been summed
    
    //if (pos == -1) return 0;
    
    if (pos==0) return a[pos];
    
    //there still are elements to sum
    //recursion
    else {
        //pos--;
        //return a[pos + 1] + sum(a, pos);
        
        return a[pos] + sum(a, pos-1);
    }
}