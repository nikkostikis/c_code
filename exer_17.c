//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

int countLetters(char *data);

//replace with main
int exer_17(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {

    
    char *s1 = malloc(150 * sizeof(char)); //dynamically allocating array of char

    int res;

    printf("Give me a string...\n");
    
    //scanf("%[^\n]s", s1); //scanf with regex to read spaces
    fgets(s1, 150, stdin); //safer than scanf
    
    //printf("\nYou typed: %s\n", s1);
    
    res = countLetters(s1);
    
    printf("The result is %d letters \n", res);
    
    return 0;
}
int countLetters(char *data) {

    int count = 0;
    int i = 0;
    int tempChar;
    for (i = 0; i <= strlen(data); i++) {
        tempChar = (int)data[i]; //ASCII equivalent of data[i]
        if ( (tempChar >= 65 && tempChar <= 90) || (tempChar >= 97 && tempChar <= 122) ) {
        //if (isdigit(data[i]) == 0 && data[i] != ' ' && data[i] != '\0' && data[i] != '\n') {
            //printf("Letter: %c \n", data[i]);
            count++; //a letter was found
        }
    }
    return count;
}
