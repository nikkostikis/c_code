//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int gcd(int a, int b);


//replace with main
int exer_25(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {

    int res, n1, n2;
    
    //read the 2 int values
    printf("Give me 2 integers...\n");
    scanf("%d", &n1);
    scanf("%d", &n2);
    
    
    res = gcd(n1, n2); //call the function
    
    printf("\nTheir gcd is: %d\n", res);
    
    return 0;
}

int gcd(int a, int b) {
    int min = a < b? a: b;
    int max = a > b? a: b;
    
    return (min == 0) ? max : gcd(min, max % min);


}