//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int equalArray(int a1[], int a2[], int pos);

//replace with main
int exer_21(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {
    
    int ar1[5] = {0, 3, 3, 4, 6};
    int ar2[5] = {0, 2, 3, 4, 6};
    int res;
    
    res = equalArray(ar1, ar2, 0);
    
    printf("Result: %d", res);
    
    
    printf("\n");
    return 0;
}

int equalArray(int a1[], int a2[], int pos) {
    //base case - non equal elements are found
    if (a1[pos] != a2[pos]) return 0;
    
    //elements in pos are equal
    else {
        //the elements tested in the current run are equal - moving on
        pos++;
        //the end of the array is reached - no non equal elements found
        if (pos == 5) return 1;
        //neither non equal elements found nor the end of the array was reached
        return equalArray(a1, a2, pos); //recursive call
    }
    
}

