//
//  testingPars.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 12/3/15.
//  Copyright © 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>

//void function(int *x, int *y);
void functionArray(int *x, int N);

//int main(int argc, const char * argv[]) {
int testingPars(int argc, const char * argv[]) {
    int a, b;
    scanf("%d", &a);
    scanf("%d", &b);
    
    int Ar[5];
    
    Ar[0] = a;
    Ar[1] = b;
    
    //functionArray(Ar, 2);
    functionArray(&a, 2);
    //for (int i = 0; i < 2; i++) {
    //    printf("%d,", Ar[i]);
    //}
    //function(&a, &b);
    printf("a = %d, b = %d \n", a,b);
    
    return 0;
    
}

void functionArray(int *x, int N) {
    for (int i = 0; i < N; i++) {
        x[i]++;
        printf("%d", x[i]);
    }
}




void function(int *x, int *y) {

    *x = *x +1;
    *y = *y +1;
    
}
