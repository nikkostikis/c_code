//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <string.h>

struct Person {
    char name[150];
    char tel[20];
};

int search7(char data[], struct Person A[], int N, char type);

//replace with main
int exer_7(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {
    
    struct Person db[3]; //maximum database size
    int count, ans, pos;
    count = -1; //the db is empty
    char data1[150];
    char data2[20];
    
    do {
        printf("\n1. Insert name and tel\n");
        printf("2. Find tel\n");
        printf("3. Find name\n");
        printf("4. Exit\n");
        scanf("%d", &ans);
        
        switch (ans) {
            case 1:
                printf("You chose to insert a name and tel.\nGive me name...\n");
                getchar(); //this is needed to eat the last \n character
                fgets(data1, 150, stdin);
                printf("Give me tel\n");
                scanf("%s", data2);
                if (count < 2) { //check if database is full
                    count++;
                    strcpy(db[count].name, data1);
                    strcpy(db[count].tel, data2);
                }
                else { //150 Persons already in database
                    printf("Can't fit any more elements in the database. Sorry\n");
                }
                break;
            case 2:
                printf("You chose to search for a tel.\nGive me the tel...\n");
                scanf("%s", data2);
                pos = search7(data2, db, count, 't');
                if (pos != -1) {
                    printf("\nName: %s\n", db[pos].name);
                }
                else {
                    printf("Sorry, nothing was found.\n");
                }
                break;
            case 3:
                printf("You chose to search for a name.\nGive me the name...\n");
                getchar(); //this is needed to eat the last \n character
                fgets(data1, 150, stdin);
                pos = search7(data1, db, count, 'n');
                if (pos != -1) {
                    printf("\nTel: %s\n", db[pos].tel);
                }
                else {
                    printf("Sorry, nothing was found.\n");
                }
                break;
            case 4:
                printf("You chose to do exit.\nBuy.\n");
                break;
            default:
                printf("Your choice was wrong\n");
                break;
        }
    } while (ans != 4);
    
    
    return 0;
}
int search7(char data[], struct Person A[], int N, char type) {
    int pos = -1;
    for (int i = 0; i <= N; i++) {
        if (strcmp(data, A[i].name) == 0 && type == 'n') {
            pos = i;
            break;
        }
        else if (strcmp(data, A[i].tel) == 0 && type == 't') {
            pos = i;
            break;
        }
    }
    return pos;
}

