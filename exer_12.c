//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

int countCapLetters(char *data);

//replace with main
int exer_12(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {

    
    //char *s1 = malloc(150 * sizeof(char)); //dynamically allocating array of char
    char s1[150];
    int res;
    
    printf("Give me a string...\n");
    
    scanf("%[^\n]s", s1); //scanf with regex to read spaces
    
    printf("\nYou typed: %s\n", s1);
    
    res = countCapLetters(s1);
    
    printf("The result is %d capital letters\n", res);
    
    return 0;
}
int countCapLetters(char *data) {
    
    int count = 0;
    int i = 0;
    
    for (i = 0; i <= strlen(data); i++) {
        if (isdigit(data[i]) == 0 && data[i] != ' ' && data[i] != '\0') {
            //printf("Letter: %c \n", data[i]);

            //if ((int)data[i] >= 65 && (int)data[i] <= 90) {
            //Type casting (i.e. changing the type) is not necessary,
            //we can compare chars directly
            if (data[i] >= 'A' && data[i] <= 'Z') {
                count++; //a capital letter was found
            }
        }
    }
    return count;
}
