//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

int countWords(char *data);

//replace with main
int exer_16(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {

    
    char *s1 = malloc(150 * sizeof(char)); //dynamically allocating array of char
    int res;
    
    printf("Give me a string...\n");
    
    fgets(s1, 150, stdin); //same functionality with scanf but safer
    //scanf("%[^\n]s", s1); //scanf with regex to read spaces
    
    res = countWords(s1);
    
    printf("The result is %d words \n", res);
    
    return 0;
}
int countWords(char *data) {

    int tempCount = 0;
    int count = 0;
    int i;
    int tempChar;
    for (i = 0; i < strlen(data); i++) { //check data one element at a time
        
        //check if data[i] == letter
        tempChar = (int)data[i]; //get ASCII equivalent
        if ( (tempChar >= 65 && tempChar <= 90) || (tempChar >= 97 && tempChar <= 122) ) {
        //if (isdigit(data[i]) == 0 && data[i] != ' ' && data[i] != '\0' && data[i] != '\n') {
            tempCount++; //a letter was found

        }
        else { //I found a non-letter (digit, blank or \0)
            if (tempCount > 0) { //check if previous was letter
                count ++; //a non letter was found immediately after letters.
                //Finalize/count the word and start a new tempCount
            }
            tempCount = 0;
        }
    }
    return count; //number of words
}
