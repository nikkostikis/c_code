//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int min(int a[], int pos, int temp);


//replace with main
int exer_22(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {

    int res, i, temp;
    int array[10];
    int size = 10;
    
    //read the 10 int values
    printf("Give me 10 integers...\n");
    for (i = 0; i < size ; i++) {
        scanf("%d", &array[i]);
    }
    
    //print the 10 int values
    printf("You gave me...\n");
    for (i = 0; i < size ; i++) {
        printf("%d\n", array[i]);
    }
    //initialize the temporary min value to the last element
    temp = array[size - 1];
    
    res = min(array, size - 2, temp); //call the function
    
    printf("\nThe minimum number in the array is: %d\n", res);
    
    return 0;
}


int min(int a[], int pos, int temp) {
    //base case all the elements have been compared
    if (pos == -1) return temp;
    //there still are elements to compare
    else {
        //change the temporary min value
        if (a[pos] < temp) temp = a[pos];
        //go to next element
        pos--;
        //recursion
        return min(a, pos, temp);
    }
}