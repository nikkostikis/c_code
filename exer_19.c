//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compStr(char *data1, char *data2);

//replace with main
int exer_19(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {

    
    //char s1[150], s2[150];
    char *s1 = malloc(150 * sizeof(char)); //dynamically allocating array of char
    char *s2 = malloc(150 * sizeof(char)); //dynamically allocating array of char
    int res;
    
    printf("Give me a string...\n");
    scanf("%s", s1);
    
    printf("Give me a second string...\n");
    scanf("%s", s2);
    
    res = compStr(s1, s2);
    
    printf("The result is: %d\n", res);
    
    return 0;
}
int compStr(char *data1, char *data2) {
    
    int i;
    int count = 0;
    for (i = 0; i < strlen(data1); i++) { //take on data1[i] at a time
        if (memchr(data2, data1[i] , sizeof(data2))) {
            //printf("The letter %c exists\n",data1[i]);
            count++;
        }
    }
    return count;
}
