//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>


//declare a function prototype
void randLetters(char cArray[], int N);

//replace with main
int exer_2(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {
    
    char A[150]; //max size of array = 150
    int N;
    
    //read a number N
    scanf("%d", &N);
    
    A[0] = 'a';
    A[1] = 'b';
    A[2] = 'c';
    A[3] = 'a';
    A[4] = 'b';
    A[5] = 'c';
    A[6] = 'a';
    A[7] = 'b';
    A[8] = 'c';
    
    
    //call the function
    randLetters(A, N);
    
    printf("%s \n", A);
    
    return 0;
}

//the function
void randLetters(char cArray[], int N){
    char c;
    //run a loop the size of N and store random letters
    for (int i = 0; i < N; i++) {
        c = 97 + rand()%26; //get random numbers from 97-122 {a-z} in ASCII
        cArray[i] = c;
    }
    //terminate the string
    cArray[N] = '\0';
}


