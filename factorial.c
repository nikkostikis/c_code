//
//  factorial.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 10/22/15.
//  Copyright © 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>

//fact method calculates x! (factorial) recursively
size_t fact(int x);

//non-recursive factorial calculation - using a loop
size_t fact_nonr(int x);

//int main(int argc, const char * argv[]) {
int factorial(int argc, const char * argv[]) {
    int n;
    
    printf("Give me an integer...\n");
    scanf("%d", &n);
    
    size_t m = fact(n); //recursive
    
    printf("Result of %d! is %zu\n", n, m);

    size_t m2 = fact_nonr(n); //non-recursive
    
    printf("Result of %d! non recursive is %zu\n", n, m2);

    
    return 0;
}

//recursive
size_t fact(int x) {
    //fact must return 1 if x == 1 - base case
    if (x == 1) {
        return 1;
    }
    //every other case where x > 1
    //it calculates recursively x*(x-1)!
    else {
        return x * fact(x-1); //recursive call
    }
}

//non-recursive
size_t fact_nonr(int x) {
    size_t p = 1;
    int i = 1;
    while (i <= x) {
        p = p * i;
        i++;
    }
    return p;
}