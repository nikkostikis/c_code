//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

int createInt(char *data);

//replace with main
int exer_14(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {

    
    char *s1 = malloc(150 * sizeof(char)); //dynamically allocating array of char
    int res;
    
    printf("Give me a string...\n");
    
    scanf("%s", s1); //scanf with regex to read spaces
    
    printf("\nYou typed: %s\n", s1);
    
    res = createInt(s1);
    
    printf("The result is %d\n", res);
    
    return 0;
}
int createInt(char *data) {
    
    int count = 0;
    int i = 0;
    
    for (i = 0; i <= strlen(data); i++) {
        count += (int)data[i];
    }
    return count;
}
