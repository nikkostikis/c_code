//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <string.h>

int isPalindrome(char* data);
int isPalindromeAlt(char* data);

//replace with main
int exer_9(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {

    
    int flag;
    char data1[150];
    
    printf("Give me a string...\n");
    scanf("%s", data1);
    printf("You gave me...\n%s\n", data1);//just checking
    
    //flag = isPalindrome(data1);
    flag = isPalindromeAlt(data1);
    printf("flag is \n%d\n", flag);

    return 0;
}


int isPalindrome(char* data) {
    int count = 0;
    char temp;
    int i, rear, end;
    char new[150];
    //first count the letters - could have used strlen(data1)
    for (i = 0; data[i] != '\0'; i++) {
        count++;
    }
    //copy the intitial input
    strcpy(new, data);
    //initialize two counters, one for the rear and one for the end of the string
    rear = 0;
    end = count - 1;
    //change the values between them, converging to the middle
    while (rear < end) {
        temp = new[rear];
        new[rear] = new[end];
        new[end] = temp;
        rear++;
        end--;
    }
    //check if the new string is the same after the reversal
    if (strcmp(new, data) == 0) {
        return 1; //means they are equal
    }
    else {
        return 0; //means they are not
    }
}

//much better - uses less memory
int isPalindromeAlt(char* data) {
    int rear = 0;
    size_t end = strlen(data) - 1;
    //compare the characters between them, converging to the middle
    while (rear < end) {
        //if I find two characters that are not equal I return 0
        if ( data[rear] != data[end] ) {
            return 0;
        }
        rear++;
        end--;
    }
    return 1;
}
