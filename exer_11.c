//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <string.h>

void reverseString(char* data);

//replace with main
int exer_11(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {
    
    
    char data1[150];
    char data2[150];
    
    printf("Give me a string...\n");
    scanf("%s", data1);
    strcpy(data2, data1); //copy the initial input to check if it works
    
    printf("You gave me...\n%s\n", data2);
    reverseString(data1);
    printf("I've turned it into...\n%s\n", data1);

    
    
    return 0;
}
void reverseString(char* data) {
    int count = 0;
    char temp;
    int i, rear, end;
    //first count the letters
    for (i = 0; data[i] != '\0'; i++) {
        count++;
    }
    
    //initialize two counters, one for the rear and one for the end of the string
    rear = 0;
    end = count - 1;
    //change the values between them, converging to the middle
    while (rear < end) {
        temp = data[rear];
        data[rear] = data[end];
        data[end] = temp;
        rear++;
        end--;
    }
}
