//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int concat(char *data1, char *data2);

//replace with main
int exer_20(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {

    
    //char s1[150], s2[150];
    char *s1 = malloc(150 * sizeof(char)); //dynamically allocating array of char
    char *s2 = malloc(150 * sizeof(char)); //dynamically allocating array of char
    int res;
    
    printf("Give me a string...\n");
    fgets(s1, 150 , stdin);
    
    printf("Give me a second string...\n");
    fgets(s2, 150, stdin);
    
    res = concat(s1, s2);
    
    printf("The resulting size is: %d\n", res);
    printf("The new string is: %s\n", s1);
    
    return 0;
}
int concat(char *data1, char *data2) {
    
    size_t i;
    int j = 0;

    for (i = strlen(data1)-1; i < (strlen(data1) + strlen(data2)) ; i++) {
        data1[i] = data2[j];
        j++;
    }
    return (int)strlen(data1);
}
