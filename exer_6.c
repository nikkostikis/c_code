//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <string.h>

//replace with main
int exer_6(int argc, const char * argv[]) {
    
    int numbers[150];
    int count, i, data, ans;
    count = -1; //the array is empty
    
    //The loop will continue until the user presses 4
    do {
        printf("1. Insert\n");
        printf("2. Display\n");
        printf("3. Delete\n");
        printf("4. Exit\n");
        scanf("%d", &ans);
        
        switch (ans) {
            case 1:
                printf("You chose to do an insertion.\nWhat would you like to insert?\n");
                scanf("%d", &data);
                if (count < 149) {
                    count++;
                    numbers[count] = data;
                }
                else {
                    printf("Can't fit any more elements in the array. Sorry\n");
                }
                break;
            case 2:
                printf("You chose to display the numbers.\nThere you go...\n");
                for (i = 0; i <= count; i++) {
                    printf("%d\n", numbers[i]);
                }
                break;
            case 3:
                printf("You chose to do a deletion.\nThe last element will be deleted.\n");
                if (count > -1) {
                    printf("Deleted element in position %d.\n", count);
                    count--;
                }
                else {
                    printf("Array is empty.\n");
                }
                break;
            case 4:
                printf("You chose to exit.\nBuy.\n");
                break;
            default:
                printf("Your choice was wrong\n");
                break;
        }
    } while (ans != 4);
    
    
    return 0;
}

