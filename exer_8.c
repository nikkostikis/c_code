//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * exclusiveString(char *data1, char *data2);

//replace with main
int exer_8(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {

    
    //char s1[150], s2[150];
    char *s1 = malloc(150 * sizeof(char)); //dynamically allocating array of char
    char *s2 = malloc(150 * sizeof(char)); //dynamically allocating array of char
    
    printf("Give me a string...\n");
    scanf("%s", s1);
    
    printf("Give me a second string...\n");
    scanf("%s", s2);
    
    s1 = exclusiveString(s1, s2);
    
    printf("The first string is now...\n%s\n", s1);
    
    return 0;
}
char * exclusiveString(char *data1, char *data2) {

    char *temp = malloc(150 * sizeof(char)); //will hold first string's characters
    int i, j;
    j = 0;
    for (i = 0; i < strlen(data1); i++) {
        //memchr searches data1[i] inside data2, that is why
        //it needs the sizeof data2 - returns true or false
        if (!memchr(data2, data1[i] , sizeof(data2))) {
            //printf("The letter %c does not exist\n",data1[i]);
            temp[j] = data1[i];
            j++;
        }
    }
    temp[j] = '\0';//just to be safe - close the string
    return temp;//this is a pointer to the temp array
}
