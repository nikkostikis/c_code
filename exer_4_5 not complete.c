//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <string.h>

struct Letters {
    char grLetter[3];
    char engLetter[3];
};

int searchEngSS(char eS[], struct Letters A[]);
int searchGrSS(char grS[], struct Letters A[]);

//replace with main
int exer_4_5(int argc, const char * argv[]) {
    
    //char engS[3];
    char grS[3];
    
    int pos, i;
    
    struct Letters lArray[5];
    strcpy(lArray[0].engLetter, "a");
    strcpy(lArray[0].grLetter, "α");
    strcpy(lArray[1].engLetter, "v");
    strcpy(lArray[1].grLetter, "β");
    strcpy(lArray[2].engLetter, "g");
    strcpy(lArray[2].grLetter, "γ");
    strcpy(lArray[3].engLetter, "d");
    strcpy(lArray[3].grLetter, "δ");
    strcpy(lArray[4].engLetter, "e");
    strcpy(lArray[4].grLetter, "ε");
    
    /*
     //read an english string
     printf("greek character...\n");
     scanf("%s", engS);
     
     
     pos = searchEngSS(engS, lArray);
     
     if (pos > -1) {
     printf("english %s, maps to greek %s \n", engS, lArray[pos].grLetter);
     }
     else {
     printf("Can't map %s to a greek letter \n", engS);
     }

    */
    
    char wordGr[50];
    
    printf("enter a greek word \n");
    scanf("%s",wordGr);
    printf("the word is...\n%s\n",wordGr);
    for (i = 0; wordGr[i] != '\0'; i++) {
        printf("%c\n", wordGr[i]);
    }


    
    //read a greek string
    printf("greek character...\n");
    scanf("%s", grS);
    
    
    pos = searchGrSS(grS, lArray);
    
    if (pos > -1) {
        printf("greek %s, maps to english %s \n", grS, lArray[pos].engLetter);
    }
    else {
        printf("Can't map %s to an english letter \n", grS);
    }
    
    return 0;
}

int searchEngSS(char engS[], struct Letters A[]) {
    int pos = -1;
    for (int i = 0; i < 5; i++) {
        if (strcmp(engS, A[i].engLetter) == 0) {
            pos = i;
            break;
        }
    }
    return pos;
}

int searchGrSS(char grS[], struct Letters A[]) {
    int pos = -1;
    for (int i = 0; i < 5; i++) {
        if (strcmp(grS, A[i].grLetter) == 0) {
            pos = i;
            break;
        }
    }
    return pos;
}

