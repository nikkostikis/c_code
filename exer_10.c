//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

//returns a number or -999 if data can't be converted to a number
int numericString(char *data);

//replace with main
int exer_10(int argc, const char * argv[]) {
//int main(int argc, const char * argv[]) {

    
    //char s1[150], s2[150];
    char *s1 = malloc(150 * sizeof(char)); //dynamically allocating array of char
    int res;
    
    printf("Give me a string...\n");
    scanf("%s", s1);
    
    res = numericString(s1);
    
    //-999 is a value stating that s1 was not a number
    if (res != -999) {
    printf("The string was numeric...\n%d\n", res);
    }
    else {
        printf("The string was not numeric...\n");
    }
    
    return 0;
}
int numericString(char *data) {

    int temp = 0;
    int i;
    size_t tenthPower = strlen(data);
    int sum = 0;
    
    for (i = 0; i < strlen(data); i++) {
        if (isdigit(data[i]) != 0) {
            temp = data[i] - '0'; //this returns the numeric equivalent of data[i]
            tenthPower --;
            sum += temp * pow(10, tenthPower); //construct the number using powers of 10

        }
        else {//I found a non digit char
            return -999; //a non numeric data[i] was found
        }
    }
    return sum;
}
