//
//  main.c
//  nikCExers
//
//  Created by Nikolaos Kostikis on 9/1/15.
//  Copyright (c) 2015 Nikolaos Kostikis. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Letter struct contains two fields, a GR string and its equivalent ENG translation
struct Letters {
    char grLetter[6];
    char engLetter[3];
};

int searchEngS(char eS, struct Letters A[]);
//int searchGrS(char *grS, struct Letters A[]);

//replace with main
int exer_5(int argc, const char * argv[]) {
    
    char c, *engS = malloc(150 * sizeof(char));

    //char *grS = malloc(150 * sizeof(char));;
    
    int pos, i;
    
    struct Letters lArray[31]; //create an array of Letter struct
    strcpy(lArray[0].engLetter, "a");
    strcpy(lArray[0].grLetter, "α");
    strcpy(lArray[1].engLetter, "v");
    strcpy(lArray[1].grLetter, "β");
    strcpy(lArray[2].engLetter, "g");
    strcpy(lArray[2].grLetter, "γ");
    strcpy(lArray[3].engLetter, "d");
    strcpy(lArray[3].grLetter, "δ");
    strcpy(lArray[4].engLetter, "e");
    strcpy(lArray[4].grLetter, "ε");
    strcpy(lArray[5].engLetter, "z");
    strcpy(lArray[5].grLetter, "ζ");
    strcpy(lArray[8].engLetter, "i");
    strcpy(lArray[8].grLetter, "η");
    strcpy(lArray[7].engLetter, "th");
    strcpy(lArray[7].grLetter, "θ");
    strcpy(lArray[6].engLetter, "i");
    strcpy(lArray[6].grLetter, "ι");
    strcpy(lArray[9].engLetter, "k");
    strcpy(lArray[9].grLetter, "κ");
    strcpy(lArray[10].engLetter, "l");
    strcpy(lArray[10].grLetter, "λ");
    strcpy(lArray[11].engLetter, "m");
    strcpy(lArray[11].grLetter, "μ");
    strcpy(lArray[12].engLetter, "n");
    strcpy(lArray[12].grLetter, "ν");
    strcpy(lArray[13].engLetter, "x");
    strcpy(lArray[13].grLetter, "ξ");
    strcpy(lArray[14].engLetter, "o");
    strcpy(lArray[14].grLetter, "ο");
    strcpy(lArray[15].engLetter, "p");
    strcpy(lArray[15].grLetter, "π");
    strcpy(lArray[16].engLetter, "r");
    strcpy(lArray[16].grLetter, "ρ");
    strcpy(lArray[17].engLetter, "s");
    strcpy(lArray[17].grLetter, "σ");
    strcpy(lArray[18].engLetter, "t");
    strcpy(lArray[18].grLetter, "τ");
    strcpy(lArray[19].engLetter, "y");
    strcpy(lArray[19].grLetter, "υ");
    strcpy(lArray[20].engLetter, "f");
    strcpy(lArray[20].grLetter, "φ");
    strcpy(lArray[21].engLetter, "ch");
    strcpy(lArray[21].grLetter, "χ");
    strcpy(lArray[22].engLetter, "ps");
    strcpy(lArray[22].grLetter, "ψ");
    strcpy(lArray[23].engLetter, "o");
    strcpy(lArray[23].grLetter, "ω");
    strcpy(lArray[24].engLetter, "u");
    strcpy(lArray[24].grLetter, "υ");
    strcpy(lArray[25].engLetter, "b");
    strcpy(lArray[25].grLetter, "μπ");
    strcpy(lArray[26].engLetter, "c");
    strcpy(lArray[26].grLetter, "κ");
    strcpy(lArray[27].engLetter, "h");
    strcpy(lArray[27].grLetter, "η");
    strcpy(lArray[28].engLetter, "j");
    strcpy(lArray[28].grLetter, "τζ");
    strcpy(lArray[29].engLetter, "q");
    strcpy(lArray[29].grLetter, "κ");
    strcpy(lArray[30].engLetter, "w");
    strcpy(lArray[30].grLetter, "ω");

    
    
    
    //read a greeklish string
    printf("greeklish phrase...\n");
    
//    scanf("%[^\n]s", engS);
    fgets(engS, 150, stdin);
    
    for (i = 0; i < strlen(engS) ; i++) {
        c = engS[i];
        pos = searchEngS(c, lArray);
        if (pos > -1) {
            printf("%s ", lArray[pos].grLetter);
        }
        else {
            printf(" ");
        }
    }
    printf("\n");
    return 0;
}

int searchEngS(char eS, struct Letters A[]) {
    //int pos = -1;
    char temp[2];
    temp[0] = eS;
    temp[1] = '\0'; //terminate the string
    for (int i = 0; i < 31; i++) {
        if (strcmp(temp, A[i].engLetter) == 0) {
            //pos = i;
            //break;
            return i;
        }
    }
//    return pos;
    return -1;
}
/*
 int searchGrS(char *grS, struct Letters A[]) {
 int pos = -1;
 for (int i = 0; i < 24; i++) {
 if (strcmp(grS, A[i].grLetter) == 0) {
 pos = i;
 break;
 }
 }
 return pos;
 }
 */
